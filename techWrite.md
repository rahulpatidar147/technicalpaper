![](image/cacheImage.jpeg)
# **Caching**
## What is Caching?

 In computing, a cache is a high-speed data storage layer that stores a subset of data, typically transient, so that future requests for that data are served up faster than is possible by accessing the data’s primary storage location. Caching allows you to efficiently reuse previously retrieved or computed data.

## How Does Caching Work?

Cached data works by storing data for re-access in a device’s memory. The data is stored high up in a computer’s memory just below the central processing unit (CPU). It is stored in a few layers, with the primary cache level built into a device’s microprocessor chip, then two more secondary levels that feed the primary level. This data is stored until it's time to live (TTL), which indicates how long content needs to be cached for, expires or the device’s disk or hard drive cache fills up.

Data is typically cached in two ways, through browser or memory caching or through CDNs.

1. Browser and memory caching: Memory caches store data locally on the computer that an application or browser runs on. When the browser is active, the resources it retrieves are stored in its random access memory (RAM) or its hard drive. The next time the resources are needed to load a webpage, the browser pulls them from the cache rather than a remote server, which makes it quicker to retrieve resources and load the page.
2. CDNs: Caching is one job of a CDN, which stores data in geographically distributed locations to reduce load times, handle vast amounts of traffic, and protect against cyberattacks. Browser requests get routed to a local CDN, which shortens the distance that response data travels and transfers resources faster.

## Caching Overview
**RAM and In-Memory Engines:** Due to the high request rates or IOPS (Input/Output operations per second) supported by RAM and In-Memory engines, caching results in improved data retrieval performance and reduces cost at scale. To support the same scale with traditional databases and disk-based hardware, additional resources would be required. These additional resources drive up the cost and still fail to achieve the low latency performance provided by an In-Memory cache.

**Applications:** Caches can be applied and leveraged throughout various layers of technology including Operating Systems, Networking layers including Content Delivery Networks (CDN) and DNS, web applications, and Databases. You can use caching to significantly reduce latency and improve IOPS for many read-heavy application workloads, such as Q&A portals, gaming, media sharing, and social networking. Cached information can include the results of database queries, computationally intensive calculations, API requests/responses and web artefacts such as HTML, JavaScript, and image files. Compute-intensive workloads that manipulate data sets, such as recommendation engines and high-performance computing simulations also benefit from an In-Memory data layer acting as a cache. In these applications, very large data sets must be accessed in real-time across clusters of machines that can span hundreds of nodes. Due to the speed of the underlying hardware, manipulating this data in a disk-based store is a significant bottleneck for these applications.

**Design Patterns:** In a distributed computing environment, a dedicated caching layer enables systems and applications to run independently from the cache with their lifecycles without the risk of affecting the cache. The cache serves as a central layer that can be accessed from disparate systems with its lifecycle and architectural topology. This is especially relevant in a system where application nodes can be dynamically scaled in and out. If the cache is resident on the same node as the application or systems utilizing it, scaling may affect the integrity of the cache. In addition, when local caches are used, they only benefit the local application consuming the data. In a distributed caching environment, the data can span multiple cache servers and be stored in a central location for the benefit of all the consumers of that data.

**Caching Best Practices:** When implementing a cache layer, it’s important to understand the validity of the data being cached. A successful cache results in a high hit rate which means the data was present when fetched. A cache miss occurs when the data fetched was not present in the cache. Controls such as TTLs (Time to live) can be applied to expire the data accordingly. Another consideration may be whether or not the cache environment needs to be Highly Available, which can be satisfied by In-Memory engines such as Redis. In some cases, an In-Memory layer can be used as a standalone data storage layer in contrast to caching data from a primary location. In this scenario, it’s important to define an appropriate RTO (Recovery Time Objective--the time it takes to recover from an outage) and RPO (Recovery Point Objective--the last point or transaction captured in the recovery) on the data resident in the In-Memory engine to determine whether or not this is suitable. Design strategies and characteristics of different In-Memory engines can be applied to meet most RTO and RPO requirements.
![image](image/technical.png)
## Caching Benefits
Effective caching aids both content consumers and content providers. Some of the benefits that caching brings to content delivery are:

**Decreased network costs:** Content can be cached at various points in the network path between the content consumer and content origin. When the content is cached closer to the consumer, requests will not cause much additional network activity beyond the cache.

**Improved responsiveness:** Caching enables content to be retrieved faster because an entire network round trip is not necessary. Caches maintained close to the user, like the browser cache, can make this retrieval nearly instantaneous.

**Increased performance on the same hardware:** For the server where the content originated, more performance can be squeezed from the same hardware by allowing aggressive caching. The content owner can leverage the powerful servers along the delivery path to take the brunt of certain content loads.

**Availability of content during network interruptions:** With certain policies, caching can be used to serve content to end-users even when it may be unavailable for short periods from the origin servers.


## Reference
1. https://medium.datadriveninvestor.com/all-things-caching-use-cases-benefits-strategies-choosing-a-caching-technology-exploring-fa6c1f2e93aa

2. https://aws.amazon.com/caching/